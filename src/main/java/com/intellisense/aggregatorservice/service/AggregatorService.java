package com.intellisense.aggregatorservice.service;

import org.json.JSONException;

public interface AggregatorService {

	public String aggregateTimeSeriesData(String inputDataSet, Integer period) throws JSONException;
}
