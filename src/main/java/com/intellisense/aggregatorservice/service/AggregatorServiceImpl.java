package com.intellisense.aggregatorservice.service;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class AggregatorServiceImpl implements AggregatorService {

	Logger logger = LoggerFactory.getLogger(AggregatorServiceImpl.class);

	public String aggregateTimeSeriesData(String inputDataSet, Integer period) throws JSONException {

		JSONObject importedObj = new JSONObject(inputDataSet);
		JSONObject importedObj660 = importedObj.getJSONObject("660");
		JSONArray importedObjArray3000 = importedObj660.getJSONArray("3000");
		JSONArray importedObjArray3001 = importedObj660.getJSONArray("3001");
		JSONArray importedObjArray3002 = importedObj660.getJSONArray("3002");
		JSONArray importedObjArray3003 = importedObj660.getJSONArray("3003");
		JSONArray importedObjArray3004 = importedObj660.getJSONArray("3004");
		JSONArray importedObjArray3005 = importedObj660.getJSONArray("3005");
		JSONArray importedObjArrayTime = importedObj660.getJSONArray("time");


		JSONObject averagedObj = new JSONObject();
		JSONObject averagedObj660 = new JSONObject();

		try {
			Field changeMap = averagedObj660.getClass().getDeclaredField("map");
			changeMap.setAccessible(true);
			changeMap.set(averagedObj660, new LinkedHashMap<>());
			changeMap.setAccessible(false);
		} catch (IllegalAccessException | NoSuchFieldException e) {
			logger.error(e.getMessage());
		}

		JSONArray averagedObjArray3000 = averageDataAsPerTimePeriod(importedObjArray3000, period);
		JSONArray averagedObjArray3001 = averageDataAsPerTimePeriod(importedObjArray3001, period);
		JSONArray averagedObjArray3002 = averageDataAsPerTimePeriod(importedObjArray3002, period);
		JSONArray averagedObjArray3003 = averageDataAsPerTimePeriod(importedObjArray3003, period);
		JSONArray averagedObjArray3004 = averageDataAsPerTimePeriod(importedObjArray3004, period);
		JSONArray averagedObjArray3005 = averageDataAsPerTimePeriod(importedObjArray3005, period);
		JSONArray averagedObjArrayTime = adjustTimeDataAsPerPeriod(importedObjArrayTime, period);

		averagedObj660.put("3000", averagedObjArray3000);
		averagedObj660.put("3001", averagedObjArray3001);
		averagedObj660.put("3002", averagedObjArray3002);
		averagedObj660.put("3003", averagedObjArray3003);
		averagedObj660.put("3004", averagedObjArray3004);
		averagedObj660.put("3005", averagedObjArray3005);
		averagedObj660.put("time", averagedObjArrayTime);
		averagedObj.put("660", averagedObj660);

		return averagedObj.toString();
	}

	private JSONArray averageDataAsPerTimePeriod(JSONArray metricArray, Integer period) throws JSONException {

		JSONArray averagedMetricArray = new JSONArray();

		DecimalFormat df = new DecimalFormat("#.##########");

		Double avgVal = 0.0000000000;
		Double sumVal = 0.0000000000;

		for(int i=0; i < metricArray.length()-1; i=i+period) {

			avgVal = 0.0000000000;
			sumVal = 0.0000000000;

			for(int j=i; j < i+period; j++) {
				sumVal = sumVal + (metricArray.isNull(j) ? 0.0000000000 : Double.valueOf(df.format(metricArray.getDouble(j))));
			}

			avgVal = Double.valueOf(df.format(sumVal)) / period;
			averagedMetricArray.put(Double.valueOf(df.format(avgVal)));
		}

		return averagedMetricArray;

	}

	private JSONArray adjustTimeDataAsPerPeriod(JSONArray metricArray, Integer period) throws JSONException {

		JSONArray averagedMetricArray = new JSONArray();

		for(int i=0; i < metricArray.length()-1; i=i+period) {
			averagedMetricArray.put(metricArray.getString(i));
		}

		return averagedMetricArray;

	}
}
