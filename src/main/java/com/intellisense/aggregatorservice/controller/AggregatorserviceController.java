package com.intellisense.aggregatorservice.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.intellisense.aggregatorservice.service.AggregatorService;



@RestController
public class AggregatorserviceController {

	Logger logger = LoggerFactory.getLogger(AggregatorserviceController.class);

	@Autowired
	private AggregatorService aggregatorService;

	@PostMapping(value = "/aggregatorservice", produces = MediaType.APPLICATION_JSON_VALUE)
	public String aggregateTimeSeriesData(@RequestBody Map<String, Object> payload) throws Exception {

		String resultJsonString = "";

		logger.info("Payload ::> " + payload);

		String periodValStr = payload.get("period") != null ? payload.get("period").toString() : "";

		if(!periodValStr.isEmpty() && (periodValStr.equals("10") || periodValStr.equals("30") || periodValStr.equals("60")))
		{
			Integer period = Integer.valueOf(periodValStr);

			logger.info("Period ::> " + period);

			// Accessing the input data set from below url
			URL url = new URL ("https://reference.intellisense.io/test.dataprovider");

			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			con.setRequestMethod("GET");

			con.setRequestProperty("Content-Type", "application/json; utf-8");
			con.setRequestProperty("Accept", "application/json");

			con.setDoOutput(true);

			int code = con.getResponseCode();
			logger.info("Response Code ::> " + code);

			String dataset = "";

			try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))){
				StringBuilder response = new StringBuilder();
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response.append(responseLine.trim());
				}

				dataset = response.toString();

				logger.info("Input Data Set >>> " + dataset);

			}

			if(!dataset.isEmpty()) {
				resultJsonString = aggregatorService.aggregateTimeSeriesData(dataset, period);
			}
			else {
				resultJsonString = "Input Data Set Is Empty";
			}
		}
		else
		{
			resultJsonString = "Period Value is Empty or Invalid >>> " + periodValStr;
		}

		logger.info("resultJsonString >>> " + resultJsonString);
		return resultJsonString;

	}
}
