package com.intellisense.aggregatorservice;

import static org.junit.Assert.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AggregatorserviceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AggregatorserviceControllerIntTest {

	@LocalServerPort
	private int port;

	TestRestTemplate restTemplate = new TestRestTemplate();

	HttpHeaders headers = new HttpHeaders();

	@Test
	public void aggregateTimeSeriesDataTestWithPeriod10() throws JSONException, URISyntaxException {

		RestTemplate restTemplate = new RestTemplate();

		final String baseUrl = createURLWithPort("/aggregatorservice");
		URI uri = new URI(baseUrl);

		HttpHeaders headers = new HttpHeaders();
		List mediaTypeLst = new ArrayList();
		mediaTypeLst.add(MediaType.APPLICATION_JSON);
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(mediaTypeLst);

		HttpEntity<String> requestEntity = new HttpEntity<>("{\"period\":10}", headers);

		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.POST, requestEntity, String.class);

		//Verify request succeed
		Assert.assertEquals(200, result.getStatusCodeValue());

		JSONObject resultObj = new JSONObject(result.getBody().toString());
		JSONObject resultObj660 = resultObj.getJSONObject("660");
		JSONArray resultObjArray3000 = resultObj660.getJSONArray("3000");
		JSONArray resultObjArray3001 = resultObj660.getJSONArray("3001");
		JSONArray resultObjArray3002 = resultObj660.getJSONArray("3002");
		JSONArray resultObjArray3003 = resultObj660.getJSONArray("3003");
		JSONArray resultObjArray3004 = resultObj660.getJSONArray("3004");
		JSONArray resultObjArray3005 = resultObj660.getJSONArray("3005");
		JSONArray resultObjArrayTime = resultObj660.getJSONArray("time");

		assertEquals(7, resultObj660.length());
		assertEquals(18, resultObjArray3000.length());
		assertEquals(18, resultObjArray3001.length());
		assertEquals(18, resultObjArray3002.length());
		assertEquals(18, resultObjArray3003.length());
		assertEquals(18, resultObjArray3004.length());
		assertEquals(18, resultObjArray3005.length());
		assertEquals(18, resultObjArrayTime.length());
	}

	@Test
	public void aggregateTimeSeriesDataTestWithPeriod30() throws JSONException, URISyntaxException {

		RestTemplate restTemplate = new RestTemplate();

		final String baseUrl = createURLWithPort("/aggregatorservice");
		URI uri = new URI(baseUrl);

		HttpHeaders headers = new HttpHeaders();
		List mediaTypeLst = new ArrayList();
		mediaTypeLst.add(MediaType.APPLICATION_JSON);
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(mediaTypeLst);

		HttpEntity<String> requestEntity = new HttpEntity<>("{\"period\":30}", headers);

		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.POST, requestEntity, String.class);

		//Verify request succeed
		Assert.assertEquals(200, result.getStatusCodeValue());

		JSONObject resultObj = new JSONObject(result.getBody().toString());
		JSONObject resultObj660 = resultObj.getJSONObject("660");
		JSONArray resultObjArray3000 = resultObj660.getJSONArray("3000");
		JSONArray resultObjArray3001 = resultObj660.getJSONArray("3001");
		JSONArray resultObjArray3002 = resultObj660.getJSONArray("3002");
		JSONArray resultObjArray3003 = resultObj660.getJSONArray("3003");
		JSONArray resultObjArray3004 = resultObj660.getJSONArray("3004");
		JSONArray resultObjArray3005 = resultObj660.getJSONArray("3005");
		JSONArray resultObjArrayTime = resultObj660.getJSONArray("time");

		assertEquals(7, resultObj660.length());
		assertEquals(6, resultObjArray3000.length());
		assertEquals(6, resultObjArray3001.length());
		assertEquals(6, resultObjArray3002.length());
		assertEquals(6, resultObjArray3003.length());
		assertEquals(6, resultObjArray3004.length());
		assertEquals(6, resultObjArray3005.length());
		assertEquals(6, resultObjArrayTime.length());

	}

	@Test
	public void aggregateTimeSeriesDataTestWithPeriod60() throws JSONException, URISyntaxException {

		RestTemplate restTemplate = new RestTemplate();

		final String baseUrl = createURLWithPort("/aggregatorservice");
		URI uri = new URI(baseUrl);

		HttpHeaders headers = new HttpHeaders();
		List mediaTypeLst = new ArrayList();
		mediaTypeLst.add(MediaType.APPLICATION_JSON);
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.setAccept(mediaTypeLst);

		HttpEntity<String> requestEntity = new HttpEntity<>("{\"period\":60}", headers);

		ResponseEntity<String> result = restTemplate.exchange(uri, HttpMethod.POST, requestEntity, String.class);

		//Verify request succeed
		Assert.assertEquals(200, result.getStatusCodeValue());

		JSONObject resultObj = new JSONObject(result.getBody().toString());
		JSONObject resultObj660 = resultObj.getJSONObject("660");
		JSONArray resultObjArray3000 = resultObj660.getJSONArray("3000");
		JSONArray resultObjArray3001 = resultObj660.getJSONArray("3001");
		JSONArray resultObjArray3002 = resultObj660.getJSONArray("3002");
		JSONArray resultObjArray3003 = resultObj660.getJSONArray("3003");
		JSONArray resultObjArray3004 = resultObj660.getJSONArray("3004");
		JSONArray resultObjArray3005 = resultObj660.getJSONArray("3005");
		JSONArray resultObjArrayTime = resultObj660.getJSONArray("time");

		assertEquals(7, resultObj660.length());
		assertEquals(3, resultObjArray3000.length());
		assertEquals(3, resultObjArray3001.length());
		assertEquals(3, resultObjArray3002.length());
		assertEquals(3, resultObjArray3003.length());
		assertEquals(3, resultObjArray3004.length());
		assertEquals(3, resultObjArray3005.length());
		assertEquals(3, resultObjArrayTime.length());

	}

	private String createURLWithPort(String uri) {
		return "http://localhost:" + port + uri;
	}
}
