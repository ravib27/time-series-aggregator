Time Series Data Aggregator Assignment


Objective:
Take time series data from a REST API endpoint and aggregate the provided data from minute values into 10 minute, 30 minute, and 1 hour averaged values based on an aggregate time period specified in the request. The data set can be accessed here: https://reference.intellisense.io/test.dataprovider


Implementation:
1.	A web service that accepts a post request with JSON data.
    a.	The post request accepts an object like: {"period":10}
    b.	The period value provides the number of minutes to average together into 1 entry
2.	On receiving a request it will use the REST endpoint above to get a set of data.
3.	This data will be averaged together into groups of data with one entry per period minutes.
4.	The output format will also be JSON and will match the import format but with less entries to reflect that entries have been averaged together.

For example the API should provide around 3 hours of data with an entry in each array per minute, so if {"period":60} is passed in for the request the output will have 3 entries in each array of the data object representing 3 time points with a value representing 60 averaged time points each.


Docker Image Details:
A docker image (ravib27/aggregatorservice) has been created for this service and pushed to docker hub. Its accessible via following url : https://hub.docker.com/repository/docker/ravib27/aggregatorservice

Below are the steps to pull the image from docker hub and run it locally:
1. Pull the image from docker hub with following command:
    $ docker pull ravib27/aggregatorservice
2. Run the docker image locally on a container with following command:
    $ docker container run --name aggregator -p 8080:8080 -d ravib27/aggregatorservice
3. The application can be accessed using Postman client with following url : http://localhost:8080/aggregatorservice
Also set the Accept header value as "application/json" and Request Body value which could be {"period":10}, {"period":30} or {"period":60} before invoking the service.
